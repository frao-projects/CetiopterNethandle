class CompilerSpec {
    [string]$CompilerName
    [string]$CCompiler
    [string]$CPPCompiler
    $InstallData

    CompilerSpec(
        [string]$CompilerName,
        [string]$CCompiler,
        [string]$CPPCompiler
    ) {
        $this.CompilerName = $CompilerName
        $this.CCompiler = $CCompiler
        $this.CPPCompiler = $CPPCompiler
        $this.InstallData = $null
    }
    CompilerSpec(
        [string]$CompilerName,
        [string]$CCompiler,
        [string]$CPPCompiler,
        $InstallData
    ) {
        $this.CompilerName = $CompilerName
        $this.CCompiler = $CCompiler
        $this.CPPCompiler = $CPPCompiler
        $this.InstallData = $InstallData
    }
}

enum InstallTypes {
    # Use meson defaults for install location
    NoInstall = 0
    # Install to default location of root/project/compiler/config
    Install = 1
}

function Confirm-InstallType {
    param (
        [Parameter(Mandatory=$true)]
        [string]$Value
    )
    $local:EnumValues = [InstallTypes].GetEnumValues()

    return ($EnumValues -contains $Value)
}

function Confirm-Directory {
	param (
		[Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -PathType Container -IsValid})]
		[string]$DirectoryName
	)

	#check if path already exists
	if(-not (Test-Path -Path $DirectoryName -PathType Container))
	{
		New-Item -ItemType Directory -Force -Path $DirectoryName | Out-Null
	}
}

function Test-CommandExists {
    param($Command)

    $OldErrPref = $ErrorActionPreference
    $ErrorActionPreference = 'stop'

    try{
        if(Get-Command $Command) {
            return $True
        }
    } catch {
        return $False
    } finally {
        $ErrorActionPreference = $OldErrPref
    }
}

#Get the data of the latest visual studio install that supports x86_64 vc++
function Request-VCx64Info {
    if(-not($IsWindows)) {
        throw "Visual Studio information requested, on non-windows OS"
    }

    $local:VSWherePath="${Env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe"

    if(-not (Test-Path -Path $VSWherePath)) {
        $local:ErrMessage=
        "Could not find VsWhere.exe at '$VSWherePath'; " +
            "Do you have a recent Visual Studio correctly installed?"
        throw [System.IO.FileNotFoundException] $ErrMessage
    }
   
    #Get actual VS install data
    . "$VSWherePath" -nologo -latest -requires Microsoft.VisualStudio.Component.VC.Tools.x86.x64 |
        Set-Variable -name arraydata

    #Format VS install data as a hashtable
    $local:VsWhereOut = ($arraydata -join "`r`n") -replace "\\", "\\" |
        ConvertFrom-StringData -Delimiter ':'

    return $VsWhereOut
}

function Initialize-VSEnvironment {
    param (
        [Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -PathType Container})]
        [string]
        $VsInstallPath
    )

    $UsedBatch = "$VsInstallPath\Common7\Tools\VsDevCmd.bat"

    if(-not (Test-Path -Path $UsedBatch)) {
        $local:ErrMessage=
        "Could not find VsDevCmd.bat at '$UsedBatch'; " +
            "Do you have a recent Visual Studio correctly installed?"
        throw [System.IO.FileNotFoundException] $ErrMessage
    }
        
    #Once we have the batch file, we run it in a cmd prompt, and return the environment afterwards.
    #With the full environment, we make sure backslashes are properly escaped, and transform it to
    #A hashtable 'FullEnv'
    cmd /c " `"$UsedBatch`" -arch=amd64 -host_arch=amd64 -no_logo && set" |
    ForEach-Object {$_ -replace "\\", "\\"} | ConvertFrom-StringData | Set-Variable -Name FullEnv

    #Iterate over each environment variable captured from the batch file, and then:
    # - Add new environment variables
    # - Change existing environment variables (probably mostly just PATH)
    foreach($var in $FullEnv.Keys) {
        if(-not (Test-Path -Path Env:$var)) {
            Set-Content -Path Env:$var -Value $FullEnv.$var
        } else {
            $local:ExistingValue=(Get-ChildItem Env:$var).Value
            
            if($ExistingValue -ne $FullEnv.$var) {
                Set-Content -Path Env:$var -Value $FullEnv.$var
            }
        }
    }
}

function Test-InstalledCompilers {
    param ()

    [CompilerSpec[]]$Local:Specs = @()

    $local:VsWhereOut | Out-Null

    try {
        #Check VS - via vswhere
        $VsWhereOut = Request-VCx64Info
    } catch{
        throw "Unable to determine if Visual studio is installed. Cannot procede"
    }

    if($VsWhereOut.catalog_productDisplayVersion -match "^(\d+)\.(\d+)\.(\d+)") {
        if(($Matches.count -gt 1) -and (([Int32]$Matches[1]) -ge 16)) {
            #we have visual studio of at least version 16.0. That is to say, VS2019
            $Specs += [CompilerSpec]::new("VS", "cl", "cl", $VsWhereOut)
        } else {
            #we have visual studio, without c++, or before 16.0
            throw "A visual studio install was found, but it was not usable, due to either being older than VS2019, or not having c++ x64 components installed. Cannot procede"
        }
    } else {
        throw "Unrecognised vswhere output. Unable to use Visual studio, and thus cannot procede"
    }
    
    return $Specs
}

function Read-OSString {
    param ()

    if($IsWindows) {
        return "windows"
    } elseif ($IsLinux) {
        return "linux"
    } else {
        return "unknownOS"
    }
}


#will write compiler and config data, to (OS)_compiler_data.json file
function Write-CompilerData {
    param(
        [Parameter(Mandatory=$true)]
        [CompilerSpec[]]$CompilerSpecs,
        [Parameter(Mandatory=$true)]
        [string[]]$ConfigList,
        [Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -IsValid})]
        [string]$FilePath
    )
        
    $local:buildData = @{
        'CompilerData' = $CompilerSpecs
        'ConfigList' = $ConfigList
    }

    $buildData | ConvertTo-Json -Depth 3 | Out-File -FilePath "$FilePath"
}

#will return simple list of compiler names, from (OS)_compiler_data.json file data
function Get-CompilerNamesList {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]$FileData
    )

    $local:CompilerSpecs = ($FileData | ConvertFrom-Json -AsHashtable -Depth 4).CompilerData

    [string[]]$local:result = @()
    foreach($local:Spec in $CompilerSpecs) {
        $result += $Spec.CompilerName
    }

    return $result
}

#will return simple list of config types, from (OS)_compiler_data.json file data
function Get-ConfigList {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]$FileData
    )

    $local:ConfigList = ($FileData | ConvertFrom-Json -AsHashtable -Depth 4).ConfigList

    [string[]]$local:result = @()
    foreach($local:Config in $ConfigList) {
        $result += $Config
    }

    return $result
}

#Will setup any compiler environment necessary (eg: the Visual studio command line environment)
function Confirm-CompilerEnvironment {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]$FileData
    )

    $local:CompilerSpecs = ($FileData | ConvertFrom-Json -AsHashtable -Depth 4).CompilerData
    foreach($local:Compiler in $CompilerSpecs) {
        if($Compiler.CompilerName -eq "VS") {
            try {
                $local:VsInstallPath = $Compiler.InstallData.installationPath

                Initialize-VSEnvironment -VsInstallPath $VsInstallPath
            } catch {
                Write-Error "Error setting Visual studio command line environment"
            }
        }
    }
}
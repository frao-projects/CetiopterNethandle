using Module .\BuildFunctions.psm1

param(
	# The Folder that contains the meson.build root file
	[Parameter(Mandatory=$true)]
	[ValidateScript({
		if(Test-Path -Path $_ -PathType Container) {
			$true
		} else {
			Write-Error "No such Source Folder!"
			exit
		}
	})]
	[string] $SourceFolder,
	# The Folder into which to create the Build folders
	[Parameter(Mandatory=$true)]
	[string] $BuildFolder,
	# A list of extra arguments to pass to meson configure
	[Parameter(Mandatory=$false, ValueFromRemainingArguments = $true)]
	[string[]] $ExtraArgs,
	# Controls if we should override meson default install location,
	# and if so, to where (relative to root)?
	# Must pass string matching the name of an 'InstallTypes' enumeration member
	[Parameter(Mandatory=$true)]
	[ValidateScript({
		if(Confirm-InstallType -Value $_) {
			$true
		} else {
			throw "$_ is not a member of the InstallTypes enum"
		}
	})]
	[string] $InstallType,
	# A path to a root install directory. The directory which will hold compiler install
	# folders (depending on InstallType)
	[string] $InstallRoot
)

#Since we can't use the enum in the param list above, we convert it to enum here
[InstallTypes]$local:InstallType = [InstallTypes]$InstallType

[string]$FraoDir = $PSScriptRoot

# confirm that we have access to meson
if(-not(Get-Command -Name "meson" -CommandType Application))
{
	throw "Could not find program: 'meson'. Please ensure specified program exists in PATH"
}

# Make sure that our BuildFolder exists
Confirm-Directory -DirectoryName $BuildFolder

#Get fully qualified paths to Source and Build folders, and tell the user what they are
# (for any debugging on their end)
$local:realSourceFolder = Resolve-Path -Path $SourceFolder
$local:realBuildFolder = Resolve-Path -Path $BuildFolder

Write-Host "Source folder: $realSourceFolder"
Write-Host "Build folder: $realBuildFolder"

#Get a list of all detected installed compilers
$local:compilerSpecs = Test-InstalledCompilers

# guarantee that our list of configurations is valid
[string[]]$local:realConfigList = @("debug", "release")

#get our operating system
$local:usedOS = Read-OSString
Write-CompilerData -CompilerSpecs $compilerSpecs -ConfigList $realConfigList -FilePath "$FraoDir/${usedOS}_compiler_data.json"

#forward declare some variables we need
$local:mesonAdditionalArgs
[boolean]$local:HaveSetupVS=$False;

foreach($local:compiler in $compilerSpecs)
{
	$usedCompilerName = $compiler.CompilerName

	if(($usedCompilerName -eq "VS") -and (-not($HaveSetupVS))) {
		#In this case we need to set up a visual studio command line environment
		$local:VSInfo = $compiler.InstallData

		if(-not($VSInfo)) {
			$VSInfo = Request-VCx64Info
		}

		try {
			$local:VsInstallPath = $VSInfo.installationpath

			Initialize-VSEnvironment -VsInstallPath $VsInstallPath
			$HaveSetupVS = $True
		} catch {}
	}

	# Set the environment variables meson uses to determine the compiler to use
	$Env:CC = $compiler.CCompiler
	$Env:CXX = $compiler.CPPCompiler

	foreach($local:config in $realConfigList)
	{
		# Set the meson arguments that we can set unconditionally.
		# This has to go here, so that we reset it every time.
		# The cpp_eh option is mandated, since we're compiling with
		# /clr, and all 's' options are incompatible with /clr
		$local:mesonArgs = @("--warnlevel", "3", "--libdir", "lib", "-Dfrao_debug_symbols=true",
			"-Dfrao_dir=$FraoDir", "-Dcpp_eh=a")

		if(-not(Test-Path -Path $InstallRoot -PathType Container -IsValid)) {
			$InstallType = [InstallTypes]::NoInstall
		}

		switch ($InstallType) {
			[InstallTypes]::Install {
				$mesonArgs += "--prefix"
				$mesonArgs += "$InstallRoot/$usedCompilerName/$config"
				break
			}
			Default {
			}
		}

		if($config -eq "debug")
		{
			$mesonArgs += "-Ddebug=true"
			$mesonArgs += "-Db_ndebug=false"
			$mesonArgs += @("--optimization", "g")
			$mesonArgs += "-Db_vscrt=mdd"
		}
		else {
			$mesonArgs += "-Ddebug=false"
			$mesonArgs += "-Db_ndebug=true"
			$mesonArgs += @("--optimization", "3")
			$mesonArgs += "-Db_vscrt=md"
		}

		foreach($local:extra in $ExtraArgs)
		{
			if($extra -ne "")
			{
				$mesonArgs += "-$extra"
			}
		}

		# Get the actual path to this config's build sub-dir
		[string]$local:resultPath = "$realBuildFolder/$usedOS/$usedCompilerName/$config"
		Confirm-Directory -DirectoryName $resultPath

		Write-Host $mesonArgs

		# Do actual meson setup, for this configuration
		. meson setup @mesonArgs $resultPath $realSourceFolder

		if($?) {
		. meson compile -C $resultPath ./Build_Paths
		}

		if($?) {
			Write-Host ""
			Write-Host "Configuration '$usedOS/$usedCompilerName/$config' Done successfully!"
			Write-Host ""
		}
	}
}
option('frao_debug_symbols', type: 'boolean', value : true)
option('frao_dir', type: 'string', yield: true)
option('frao_cpp_std', type: 'integer', min: 14, max: 17, value: 17, yield: true)
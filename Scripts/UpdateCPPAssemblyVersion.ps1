param (
    [Parameter(Mandatory=$true)]
    [string]
    $AssemblyFile,
    [Parameter(Mandatory=$true)]
    [string]
    $Version
)

$FileContent = (Get-Content -LiteralPath $AssemblyFile)
$FileContent = $FileContent -replace "^\[assembly:AssemblyVersionAttribute\(\`"(.+)\`"\)\];$",
 "[assembly:AssemblyVersionAttribute(`"$Version`")];"

$FileContent | Set-Content -LiteralPath $AssemblyFile
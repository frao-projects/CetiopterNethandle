#include "LinkFuncs.h"

namespace IMPL
{
void add(frao::Maths::Double4* result,
		 frao::Maths::Double4* argLhs,
		 frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorAddition(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void add(frao::Maths::Integer4* result,
		 frao::Maths::Integer4* argLhs,
		 frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorAddition(
					 frao::Maths::loadInteger4(*argLhs),
					 frao::Maths::loadInteger4(*argRhs)));
}

void subtract(frao::Maths::Double4* result,
			  frao::Maths::Double4* argLhs,
			  frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorSubtract(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void subtract(frao::Maths::Integer4* result,
			  frao::Maths::Integer4* argLhs,
			  frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorSubtract(
					 frao::Maths::loadInteger4(*argLhs),
					 frao::Maths::loadInteger4(*argRhs)));
}

void multiply(frao::Maths::Double4* result,
			  frao::Maths::Double4* argLhs,
			  frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorMultiply(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void multiply(frao::Maths::Integer4* result,
			  frao::Maths::Integer4* argLhs,
			  frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorMultiply(
					 frao::Maths::loadInteger4(*argLhs),
					 frao::Maths::loadInteger4(*argRhs)));
}

void divide(frao::Maths::Double4* result,
			frao::Maths::Double4* argLhs,
			frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorDivide(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void divide(frao::Maths::Integer4* result,
			frao::Maths::Integer4* argLhs,
			frao::Maths::Integer4* argRhs)
{
	frao::Maths::WideIntVector remRes;

	frao::Maths::storeInteger4(
		*result,
		frao::Maths::vectorDivide(
			frao::Maths::loadInteger4(*argLhs),
			frao::Maths::loadInteger4(*argRhs), &remRes));
}

void remainder(frao::Maths::Integer4* result,
			   frao::Maths::Integer4* argLhs,
			   frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorRemainder(
					 frao::Maths::loadInteger4(*argLhs),
					 frao::Maths::loadInteger4(*argRhs)));
}

void
	and (frao::Maths::Integer4 * result,
		 frao::Maths::Integer4* argLhs,
		 frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorAnd(
					 frao::Maths::loadInteger4(*argLhs),
					 frao::Maths::loadInteger4(*argRhs)));
}
void
	or (frao::Maths::Integer4 * result,
		frao::Maths::Integer4* argLhs,
		frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorOr(
					 frao::Maths::loadInteger4(*argLhs),
					 frao::Maths::loadInteger4(*argRhs)));
}
void
	xor (frao::Maths::Integer4 * result,
		 frao::Maths::Integer4* argLhs,
		 frao::Maths::Integer4* argRhs)
{
	frao::Maths::storeInteger4(
		*result,
		frao::Maths::vectorXor(
			frao::Maths::loadInteger4(*argLhs),
			frao::Maths::loadInteger4(*argRhs)));
}
void not(frao::Maths::Integer4 * result, frao::Maths::Integer4* arg)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorNot(
					 frao::Maths::loadInteger4(*arg)));
}

void sqrt(frao::Maths::Double4* result,
		  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorSqrt(
					 frao::Maths::loadDouble4(*arg)));
}
void negate(frao::Maths::Double4* result,
			frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorNegate(
					 frao::Maths::loadDouble4(*arg)));
}
void scale(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg, double scaleFactor)
{
	frao::Maths::storeDouble4(
		*result,
		frao::Maths::vectorScale(
			frao::Maths::loadDouble4(*arg), scaleFactor));
}

void sin(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorSin(
					 frao::Maths::loadDouble4(*arg)));
}
void cos(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorCos(
					 frao::Maths::loadDouble4(*arg)));
}
void tan(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorTan(
					 frao::Maths::loadDouble4(*arg)));
}
void arcsin(frao::Maths::Double4* result,
			frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorArcSin(
					 frao::Maths::loadDouble4(*arg)));
}
void arccos(frao::Maths::Double4* result,
			frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorArcCos(
					 frao::Maths::loadDouble4(*arg)));
}
void arctan(frao::Maths::Double4* result,
			frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorArcTan(
					 frao::Maths::loadDouble4(*arg)));
}
void arctan2(frao::Maths::Double4* result,
			 frao::Maths::Double4* argTop,
			 frao::Maths::Double4* argBottom)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorArcTan2(
					 frao::Maths::loadDouble4(*argTop),
					 frao::Maths::loadDouble4(*argBottom)));
}

void exp(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorExp(
					 frao::Maths::loadDouble4(*arg)));
}
void exp2(frao::Maths::Double4* result,
		  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorExp2(
					 frao::Maths::loadDouble4(*arg)));
}
void pow(frao::Maths::Double4* result,
		 frao::Maths::Double4* base,
		 frao::Maths::Double4* exponent)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorPow(
					 frao::Maths::loadDouble4(*base),
					 frao::Maths::loadDouble4(*exponent)));
}
void log(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorLog(
					 frao::Maths::loadDouble4(*arg)));
}
void log2(frao::Maths::Double4* result,
		  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorLog2(
					 frao::Maths::loadDouble4(*arg)));
}
void log10(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorLog10(
					 frao::Maths::loadDouble4(*arg)));
}
void abs(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorAbs(
					 frao::Maths::loadDouble4(*arg)));
}
void min(frao::Maths::Double4* result,
		 frao::Maths::Double4* argLhs,
		 frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorMin(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void max(frao::Maths::Double4* result,
		 frao::Maths::Double4* argLhs,
		 frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorMax(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}

void round(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorRound(
					 frao::Maths::loadDouble4(*arg)));
}
void floor(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorFloor(
					 frao::Maths::loadDouble4(*arg)));
}
void ceiling(frao::Maths::Double4* result,
			 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorCeiling(
					 frao::Maths::loadDouble4(*arg)));
}
void truncate(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorTruncate(
					 frao::Maths::loadDouble4(*arg)));
}

void dot2D(frao::Maths::Double4* result,
		   frao::Maths::Double4* argLhs,
		   frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector2DDot(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void length2D(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector2DLength(
					 frao::Maths::loadDouble4(*arg)));
}
void lengthSq2D(frao::Maths::Double4* result,
				frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector2DLengthSq(
					 frao::Maths::loadDouble4(*arg)));
}
void normalise2D(frao::Maths::Double4* result,
				 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector2DNormalised(
					 frao::Maths::loadDouble4(*arg)));
}
void dot3D(frao::Maths::Double4* result,
		   frao::Maths::Double4* argLhs,
		   frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector3DDot(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void cross3D(frao::Maths::Double4* result,
			 frao::Maths::Double4* argLhs,
			 frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector3DCross(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void length3D(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector3DLength(
					 frao::Maths::loadDouble4(*arg)));
}
void lengthSq3D(frao::Maths::Double4* result,
				frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector3DLengthSq(
					 frao::Maths::loadDouble4(*arg)));
}
void normalise3D(frao::Maths::Double4* result,
				 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector3DNormalised(
					 frao::Maths::loadDouble4(*arg)));
}
void dot4D(frao::Maths::Double4* result,
		   frao::Maths::Double4* argLhs,
		   frao::Maths::Double4* argRhs)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector4DDot(
					 frao::Maths::loadDouble4(*argLhs),
					 frao::Maths::loadDouble4(*argRhs)));
}
void length4D(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector4DLength(
					 frao::Maths::loadDouble4(*arg)));
}
void lengthSq4D(frao::Maths::Double4* result,
				frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector4DLengthSq(
					 frao::Maths::loadDouble4(*arg)));
}
void normalise4D(frao::Maths::Double4* result,
				 frao::Maths::Double4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vector4DNormalised(
					 frao::Maths::loadDouble4(*arg)));
}

void toInteger(frao::Maths::Integer4* result,
			   frao::Maths::Double4*  arg)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::toIntegerVector(
					 frao::Maths::loadDouble4(*arg)));
}
void toDouble(frao::Maths::Double4*	 result,
			  frao::Maths::Integer4* arg)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::toDoubleVector(
					 frao::Maths::loadInteger4(*arg)));
}

void zero(frao::Maths::Double4* result)
{
	frao::Maths::storeDouble4(*result,
							  frao::Maths::vectorZero());
}
void zero(frao::Maths::Integer4* result)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorZeroInt());
}
void one(frao::Maths::Double4* result)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorSplatOne());
}
void one(frao::Maths::Integer4* result)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorSplatOneInt());
}
void allBits(frao::Maths::Integer4* result)
{
	frao::Maths::storeInteger4(
		*result, frao::Maths::vectorSplatAllBits());
}
void epsilon(frao::Maths::Double4* result)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorSplatEpsilon());
}
void infinity(frao::Maths::Double4* result)
{
	frao::Maths::storeDouble4(
		*result, frao::Maths::vectorSplatInfinity());
}
}  // namespace IMPL
#include "IntVector.h"
#include "LinkFuncs.h"
#include "DoubleVector.h"

namespace CetiopterNethandle
{
#pragma managed

IntVector::IntVector()
	: m_Values(new frao::Maths::Integer4())
{}
IntVector::~IntVector()
{
	// call finaliser, to avoid code duplication
	this->!IntVector();
}
IntVector::!IntVector()
{
	delete m_Values;
}

int_least64_t IntVector::X::get()
{
	return m_Values->m_Components.m_X;
}
void IntVector::X::set(int_least64_t value)
{
	m_Values->m_Components.m_X = value;
}
int_least64_t IntVector::Y::get()
{
	return m_Values->m_Components.m_Y;
}
void IntVector::Y::set(int_least64_t value)
{
	m_Values->m_Components.m_Y = value;
}
int_least64_t IntVector::Z::get()
{
	return m_Values->m_Components.m_Z;
}
void IntVector::Z::set(int_least64_t value)
{
	m_Values->m_Components.m_Z = value;
}
int_least64_t IntVector::W::get()
{
	return m_Values->m_Components.m_W;
}
void IntVector::W::set(int_least64_t value)
{
	m_Values->m_Components.m_W = value;
}

IntVector ^ IntVector::Add(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::add(result->m_Values, lhs->m_Values,
			  rhs->m_Values);

	return result;
}
IntVector
	^ IntVector::Subtract(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::subtract(result->m_Values, lhs->m_Values,
				   rhs->m_Values);

	return result;
}
IntVector
	^ IntVector::Multiply(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::multiply(result->m_Values, lhs->m_Values,
				   rhs->m_Values);

	return result;
}
IntVector
	^ IntVector::Divide(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::divide(result->m_Values, lhs->m_Values,
				 rhs->m_Values);

	return result;
}
IntVector
	^ IntVector::Remainder(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::remainder(result->m_Values, lhs->m_Values,
					rhs->m_Values);

	return result;
}

IntVector ^ IntVector::And(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::and(result->m_Values, lhs->m_Values,
			  rhs->m_Values);

	return result;
}
IntVector ^ IntVector::Or(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::
			or (result->m_Values, lhs->m_Values,
				rhs->m_Values);

	return result;
}
IntVector ^ IntVector::Xor(IntVector ^ lhs, IntVector ^ rhs)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::
			xor (result->m_Values, lhs->m_Values,
				 rhs->m_Values);

	return result;
}
IntVector ^ IntVector::Not(IntVector ^ arg)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::not(result->m_Values, arg->m_Values);

	return result;
}

DoubleVector ^ IntVector::ToDoubleVector(IntVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::toDouble(result->m_Values, arg->m_Values);

	return result;
}

IntVector ^ IntVector::Zero()
{
	IntVector ^ result = gcnew IntVector();

	IMPL::zero(result->m_Values);

	return result;
}
IntVector ^ IntVector::One()
{
	IntVector ^ result = gcnew IntVector();

	IMPL::one(result->m_Values);

	return result;
}
IntVector ^ IntVector::AllBits()
{
	IntVector ^ result = gcnew IntVector();

	IMPL::allBits(result->m_Values);

	return result;
}
}  // namespace CetiopterNethandle
#include "DoubleVector.h"
#include "LinkFuncs.h"
#include "IntVector.h"

namespace CetiopterNethandle
{
#pragma managed

DoubleVector::DoubleVector()
	: m_Values(new frao::Maths::Double4())
{}
DoubleVector::~DoubleVector()
{
	// call finaliser, to avoid code duplication
	this->!DoubleVector();
}
DoubleVector::!DoubleVector()
{
	delete m_Values;
}

double DoubleVector::X::get()
{
	return m_Values->m_Components.m_X;
}
void DoubleVector::X::set(double value)
{
	m_Values->m_Components.m_X = value;
}
double DoubleVector::Y::get()
{
	return m_Values->m_Components.m_Y;
}
void DoubleVector::Y::set(double value)
{
	m_Values->m_Components.m_Y = value;
}
double DoubleVector::Z::get()
{
	return m_Values->m_Components.m_Z;
}
void DoubleVector::Z::set(double value)
{
	m_Values->m_Components.m_Z = value;
}
double DoubleVector::W::get()
{
	return m_Values->m_Components.m_W;
}
void DoubleVector::W::set(double value)
{
	m_Values->m_Components.m_W = value;
}

DoubleVector
	^ DoubleVector::Add(DoubleVector ^ lhs,
						DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::add(result->m_Values, lhs->m_Values,
			  rhs->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Subtract(DoubleVector ^ lhs,
							 DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::subtract(result->m_Values, lhs->m_Values,
				   rhs->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Multiply(DoubleVector ^ lhs,
							 DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::multiply(result->m_Values, lhs->m_Values,
				   rhs->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Divide(DoubleVector ^ lhs,
						   DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::divide(result->m_Values, lhs->m_Values,
				 rhs->m_Values);

	return result;
}

DoubleVector ^ DoubleVector::Sqrt(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::sqrt(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Negate(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::negate(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Scale(DoubleVector ^ arg,
						  double scalefactor)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::scale(result->m_Values, arg->m_Values,
				scalefactor);

	return result;
}

DoubleVector ^ DoubleVector::Sin(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::sin(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Cos(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::cos(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Tan(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::tan(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::ArcSin(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::arcsin(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::ArcCos(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::arccos(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::ArcTan(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::arctan(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::ArcTan2(DoubleVector ^ top,
							DoubleVector ^ bottom)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::arctan2(result->m_Values, top->m_Values,
				  bottom->m_Values);

	return result;
}

DoubleVector ^ DoubleVector::Exp(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::exp(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Exp2(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::exp2(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Pow(DoubleVector ^ base,
						DoubleVector ^ exponent)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::pow(result->m_Values, base->m_Values,
			  exponent->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Log(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::log(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Log2(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::log2(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Log10(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::log10(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Abs(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::abs(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Min(DoubleVector ^ lhs,
						DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::min(result->m_Values, lhs->m_Values,
			  rhs->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Max(DoubleVector ^ lhs,
						DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::max(result->m_Values, lhs->m_Values,
			  rhs->m_Values);

	return result;
}

DoubleVector ^ DoubleVector::Round(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::round(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Floor(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::floor(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Ceiling(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::ceiling(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Truncate(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::truncate(result->m_Values, arg->m_Values);

	return result;
}

DoubleVector
	^ DoubleVector::Dot2D(DoubleVector ^ lhs,
						  DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::dot2D(result->m_Values, lhs->m_Values,
				rhs->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Length2D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::length2D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::LengthSq2D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::lengthSq2D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Normalise2D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::normalise2D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Dot3D(DoubleVector ^ lhs,
						  DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::dot3D(result->m_Values, lhs->m_Values,
				rhs->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Cross3D(DoubleVector ^ lhs,
							DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::cross3D(result->m_Values, lhs->m_Values,
				  rhs->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Length3D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::length3D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::LengthSq3D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::lengthSq3D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Normalise3D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::normalise3D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector
	^ DoubleVector::Dot4D(DoubleVector ^ lhs,
						  DoubleVector ^ rhs)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::dot4D(result->m_Values, lhs->m_Values,
				rhs->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Length4D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::length4D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::LengthSq4D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::lengthSq4D(result->m_Values, arg->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Normalise4D(DoubleVector ^ arg)
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::normalise4D(result->m_Values, arg->m_Values);

	return result;
}

IntVector ^ DoubleVector::ToIntVector(DoubleVector ^ arg)
{
	IntVector ^ result = gcnew IntVector();

	IMPL::toInteger(result->m_Values, arg->m_Values);

	return result;
}

DoubleVector ^ DoubleVector::Zero()
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::zero(result->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::One()
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::one(result->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Epsilon()
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::epsilon(result->m_Values);

	return result;
}
DoubleVector ^ DoubleVector::Infinity()
{
	DoubleVector ^ result = gcnew DoubleVector();

	IMPL::infinity(result->m_Values);

	return result;
}
}  // namespace CetiopterNethandle
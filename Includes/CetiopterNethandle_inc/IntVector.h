#pragma once

#include <Cetiopter_inc/VectorTypes.hpp>

namespace CetiopterNethandle
{
#pragma managed

//declared here bc c++/CLI desn't appear to like referring to the header file containing this
ref class DoubleVector;

public
ref class IntVector
{
	internal : frao::Maths::Integer4* m_Values;

   public:
	IntVector();
	~IntVector();
	!IntVector();

	property int_least64_t X
	{
		int_least64_t get();
		void		  set(int_least64_t value);
	}
	property int_least64_t Y
	{
		int_least64_t get();
		void		  set(int_least64_t value);
	}
	property int_least64_t Z
	{
		int_least64_t get();
		void		  set(int_least64_t value);
	}
	property int_least64_t W
	{
		int_least64_t get();
		void		  set(int_least64_t value);
	}

	static IntVector
		^ Add(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector
		^ Subtract(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector
		^ Multiply(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector
		^ Divide(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector
		^ Remainder(IntVector ^ lhs, IntVector ^ rhs);
	
	static IntVector
		^ And(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector ^ Or(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector
		^ Xor(IntVector ^ lhs, IntVector ^ rhs);
	static IntVector ^ Not(IntVector ^ arg);

	static DoubleVector ^ ToDoubleVector(IntVector ^ arg);
	
	static IntVector ^ Zero();
	static IntVector ^ One();
	static IntVector ^ AllBits();
};
}  // namespace CetiopterNethandle
#pragma once

#include <Cetiopter_inc/VectorTypes.hpp>

#pragma unmanaged

namespace IMPL
{
void add(frao::Maths::Double4* result,
		 frao::Maths::Double4* argLhs,
		 frao::Maths::Double4* argRhs);
void add(frao::Maths::Integer4* result,
		 frao::Maths::Integer4* argLhs,
		 frao::Maths::Integer4* argRhs);

void subtract(frao::Maths::Double4* result,
			  frao::Maths::Double4* argLhs,
			  frao::Maths::Double4* argRhs);
void subtract(frao::Maths::Integer4* result,
			  frao::Maths::Integer4* argLhs,
			  frao::Maths::Integer4* argRhs);

void multiply(frao::Maths::Double4* result,
			  frao::Maths::Double4* argLhs,
			  frao::Maths::Double4* argRhs);
void multiply(frao::Maths::Integer4* result,
			  frao::Maths::Integer4* argLhs,
			  frao::Maths::Integer4* argRhs);

void divide(frao::Maths::Double4* result,
			frao::Maths::Double4* argLhs,
			frao::Maths::Double4* argRhs);
void divide(frao::Maths::Integer4* result,
			frao::Maths::Integer4* argLhs,
			frao::Maths::Integer4* argRhs);

void remainder(frao::Maths::Integer4* result,
			   frao::Maths::Integer4* argLhs,
			   frao::Maths::Integer4* argRhs);

void
	and (frao::Maths::Integer4 * result,
		 frao::Maths::Integer4* argLhs,
		 frao::Maths::Integer4* argRhs);
void
	or (frao::Maths::Integer4 * result,
		frao::Maths::Integer4* argLhs,
		frao::Maths::Integer4* argRhs);
void
	xor (frao::Maths::Integer4 * result,
		 frao::Maths::Integer4* argLhs,
		 frao::Maths::Integer4* argRhs);
void not(frao::Maths::Integer4 * result,
		 frao::Maths::Integer4* arg);

void sqrt(frao::Maths::Double4* result,
		  frao::Maths::Double4* arg);
void negate(frao::Maths::Double4* result,
			frao::Maths::Double4* arg);
void scale(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg, double scaleFactor);

void sin(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg);
void cos(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg);
void tan(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg);
void arcsin(frao::Maths::Double4* result,
			frao::Maths::Double4* arg);
void arccos(frao::Maths::Double4* result,
			frao::Maths::Double4* arg);
void arctan(frao::Maths::Double4* result,
			frao::Maths::Double4* arg);
void arctan2(frao::Maths::Double4* result,
			 frao::Maths::Double4* argTop,
			 frao::Maths::Double4* argBottom);

void exp(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg);
void exp2(frao::Maths::Double4* result,
		  frao::Maths::Double4* arg);
void pow(frao::Maths::Double4* result,
		 frao::Maths::Double4* base,
		 frao::Maths::Double4* exponent);
void log(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg);
void log2(frao::Maths::Double4* result,
		  frao::Maths::Double4* arg);
void log10(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg);
void abs(frao::Maths::Double4* result,
		 frao::Maths::Double4* arg);
void min(frao::Maths::Double4* result,
		 frao::Maths::Double4* argLhs,
		 frao::Maths::Double4* argRhs);
void max(frao::Maths::Double4* result,
		 frao::Maths::Double4* argLhs,
		 frao::Maths::Double4* argRhs);

void round(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg);
void floor(frao::Maths::Double4* result,
		   frao::Maths::Double4* arg);
void ceiling(frao::Maths::Double4* result,
			 frao::Maths::Double4* arg);
void truncate(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg);

void dot2D(frao::Maths::Double4* result,
		   frao::Maths::Double4* argLhs,
		   frao::Maths::Double4* argRhs);
void length2D(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg);
void lengthSq2D(frao::Maths::Double4* result,
				frao::Maths::Double4* arg);
void normalise2D(frao::Maths::Double4* result,
				 frao::Maths::Double4* arg);
void dot3D(frao::Maths::Double4* result,
		   frao::Maths::Double4* argLhs,
		   frao::Maths::Double4* argRhs);
void cross3D(frao::Maths::Double4* result,
			 frao::Maths::Double4* argLhs,
			 frao::Maths::Double4* argRhs);
void length3D(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg);
void lengthSq3D(frao::Maths::Double4* result,
				frao::Maths::Double4* arg);
void normalise3D(frao::Maths::Double4* result,
				 frao::Maths::Double4* arg);
void dot4D(frao::Maths::Double4* result,
		   frao::Maths::Double4* argLhs,
		   frao::Maths::Double4* argRhs);
void length4D(frao::Maths::Double4* result,
			  frao::Maths::Double4* arg);
void lengthSq4D(frao::Maths::Double4* result,
				frao::Maths::Double4* arg);
void normalise4D(frao::Maths::Double4* result,
				 frao::Maths::Double4* arg);

void toInteger(frao::Maths::Integer4* result,
			   frao::Maths::Double4*  arg);
void toDouble(frao::Maths::Double4*	 result,
			  frao::Maths::Integer4* arg);

void zero(frao::Maths::Double4* result);
void zero(frao::Maths::Integer4* result);
void one(frao::Maths::Double4* result);
void one(frao::Maths::Integer4* result);
void allBits(frao::Maths::Integer4* result);
void epsilon(frao::Maths::Double4* result);
void infinity(frao::Maths::Double4* result);
}  // namespace IMPL
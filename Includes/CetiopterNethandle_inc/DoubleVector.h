#pragma once

#include <Cetiopter_inc/VectorTypes.hpp>
#include "IntVector.h"

namespace CetiopterNethandle
{
#pragma managed

// declared here bc c++/CLI desn't appear to like referring
// to the header file containing this
ref class IntVector;

public
ref class DoubleVector
{
	internal : frao::Maths::Double4* m_Values;

   public:
	DoubleVector();
	~DoubleVector();
	!DoubleVector();

	property double X
	{
		double get();
		void   set(double value);
	}
	property double Y
	{
		double get();
		void   set(double value);
	}
	property double Z
	{
		double get();
		void   set(double value);
	}
	property double W
	{
		double get();
		void   set(double value);
	}

	static DoubleVector
		^ Add(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector
		^ Subtract(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector
		^ Multiply(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector
		^ Divide(DoubleVector ^ lhs, DoubleVector ^ rhs);

	static DoubleVector ^ Sqrt(DoubleVector ^ arg);
	static DoubleVector ^ Negate(DoubleVector ^ arg);
	static DoubleVector
		^ Scale(DoubleVector ^ arg, double scalefactor);

	static DoubleVector ^ Sin(DoubleVector ^ arg);
	static DoubleVector ^ Cos(DoubleVector ^ arg);
	static DoubleVector ^ Tan(DoubleVector ^ arg);
	static DoubleVector ^ ArcSin(DoubleVector ^ arg);
	static DoubleVector ^ ArcCos(DoubleVector ^ arg);
	static DoubleVector ^ ArcTan(DoubleVector ^ arg);
	static DoubleVector
		^ ArcTan2(DoubleVector ^ top,
				  DoubleVector ^ bottom);

	static DoubleVector ^ Exp(DoubleVector ^ arg);
	static DoubleVector ^ Exp2(DoubleVector ^ arg);
	static DoubleVector
		^ Pow(DoubleVector ^ base, DoubleVector ^ exponent);
	static DoubleVector ^ Log(DoubleVector ^ arg);
	static DoubleVector ^ Log2(DoubleVector ^ arg);
	static DoubleVector ^ Log10(DoubleVector ^ arg);
	static DoubleVector ^ Abs(DoubleVector ^ arg);
	static DoubleVector
		^ Min(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector
		^ Max(DoubleVector ^ lhs, DoubleVector ^ rhs);

	static DoubleVector ^ Round(DoubleVector ^ arg);
	static DoubleVector ^ Floor(DoubleVector ^ arg);
	static DoubleVector ^ Ceiling(DoubleVector ^ arg);
	static DoubleVector ^ Truncate(DoubleVector ^ arg);

	static DoubleVector
		^ Dot2D(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector ^ Length2D(DoubleVector ^ arg);
	static DoubleVector ^ LengthSq2D(DoubleVector ^ arg);
	static DoubleVector ^ Normalise2D(DoubleVector ^ arg);
	static DoubleVector
		^ Dot3D(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector
		^ Cross3D(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector ^ Length3D(DoubleVector ^ arg);
	static DoubleVector ^ LengthSq3D(DoubleVector ^ arg);
	static DoubleVector ^ Normalise3D(DoubleVector ^ arg);
	static DoubleVector
		^ Dot4D(DoubleVector ^ lhs, DoubleVector ^ rhs);
	static DoubleVector ^ Length4D(DoubleVector ^ arg);
	static DoubleVector ^ LengthSq4D(DoubleVector ^ arg);
	static DoubleVector ^ Normalise4D(DoubleVector ^ arg);

	static IntVector ^ ToIntVector(DoubleVector ^ arg);

	static DoubleVector ^ Zero();
	static DoubleVector ^ One();
	static DoubleVector ^ Epsilon();
	static DoubleVector ^ Infinity();
};
	}  // namespace CetiopterNethandle